package com.example.financetracker;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.financetracker.context.AppDatabase;
import com.example.financetracker.context.Expense;
import com.example.financetracker.context.Income;
import com.example.financetracker.context.dao.ExpenseDAO;
import com.example.financetracker.context.dao.IncomeDAO;

import java.util.ArrayList;

public class FormFragment extends Fragment {
    Button btnSubmit;
    Spinner spinnerType;
    EditText txtAmount;
    EditText txtdescription;



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.form_fragment,container,false);
        btnSubmit = view.findViewById(R.id.submitButton);
        spinnerType= view.findViewById(R.id.spinnerType);
        txtAmount = view.findViewById(R.id.txtamount);
        txtdescription = view.findViewById(R.id.txtDescription);

        String[] items = new String[]{"EXPENSE", "INCOME"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this.getContext(), android.R.layout.simple_spinner_dropdown_item,items);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerType.setAdapter(adapter);

        AppDatabase appDb = Room.databaseBuilder(this.getContext(),AppDatabase.class,"financeTracker").allowMainThreadQueries().build();
        ExpenseDAO expenseDAO = appDb.expenseDAO();
        IncomeDAO incomeDAO = appDb.incomeDAO();
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!txtdescription.getText().toString().equals("") && !txtAmount.getText().toString().equals("")){
                    switch (spinnerType.getSelectedItem().toString().toLowerCase()){
                        case "expense":
                            Expense ex = new Expense();
                            ex.setAmount(Double.parseDouble(txtAmount.getText().toString()));
                            ex.setDescription(txtdescription.getText().toString());
                            expenseDAO.insertExpense(ex);
                            txtdescription.setText("");
                            txtAmount.setText("");
                            Toast.makeText(v.getContext(),"Expense added successfully!",Toast.LENGTH_SHORT).show();

                            break;
                        case "income":
                            Income income = new Income();
                            income.setAmount(Double.parseDouble(txtAmount.getText().toString()));
                            income.setDescription(txtdescription.getText().toString());
                            incomeDAO.insertIncome(income);
                            txtdescription.setText("");
                            txtAmount.setText("");
                            Toast.makeText(v.getContext(),"Income added successfully!",Toast.LENGTH_SHORT).show();
                            break;
                    }
                }else{
                    Toast.makeText(v.getContext(),"tous les champs sont obligatoire !",Toast.LENGTH_SHORT).show();
                }

            }
        });

        return view;
    }
}
