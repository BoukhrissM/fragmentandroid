package com.example.financetracker.context.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.financetracker.context.Expense;

import java.util.List;

@Dao
public interface ExpenseDAO {
    @Query("select * from expense order by expenseID desc")
    List<Expense> getAll();


    @Query("select * from expense where expenseID = :id")
    Expense findById(Long id);
    @Insert
    void insertExpense(Expense expense);

    @Delete
    void  delete(Expense expense);
}
