package com.example.financetracker.context.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.financetracker.context.Income;

import java.util.List;

@Dao
public interface IncomeDAO {
    @Query("select * from Income order by IncomeId desc")
    List<Income> getAll();

    @Insert
    void insertIncome(Income income);

    @Delete
    void delete(Income income);

}
