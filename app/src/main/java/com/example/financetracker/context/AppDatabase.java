package com.example.financetracker.context;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.financetracker.context.dao.ExpenseDAO;
import com.example.financetracker.context.dao.IncomeDAO;


@Database(entities = {Expense.class,Income.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract ExpenseDAO expenseDAO();
    public abstract IncomeDAO incomeDAO();

}
