package com.example.financetracker.context;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Income {
    @PrimaryKey
    private Long IncomeId;
    private Double amount;
    private String description;

    public Long getIncomeId() {
        return IncomeId;
    }

    public void setIncomeId(Long incomeId) {
        IncomeId = incomeId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
