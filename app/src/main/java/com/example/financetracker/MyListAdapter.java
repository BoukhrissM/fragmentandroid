package com.example.financetracker;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class MyListAdapter extends ArrayAdapter<String> {

    private final Activity context;
    private final String[] descriptions;
    private final String[] amounts;

    private  ArrayList<Long> Ids;

    public MyListAdapter(Activity context, String[] descriptions, String[] subtitles,ArrayList<Long> ids){
        super(context.getApplicationContext(), R.layout.list_item, descriptions);
        this.context=context;
        this.descriptions = descriptions;
        this.amounts = subtitles;
        this.Ids = ids;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View RowView = inflater.inflate(R.layout.list_item,null,true);

        TextView title = RowView.findViewById(R.id.title);
        TextView subtitle = RowView.findViewById(R.id.subtitle);

        title.setText(amounts[position]);
        subtitle.setText(descriptions[position]);
        return RowView;
    }

    public ArrayList<Long> getIds() {
        return Ids;
    }

    public void setIds(ArrayList<Long> ids) {
        Ids = ids;
    }
}
