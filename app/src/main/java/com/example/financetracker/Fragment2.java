package com.example.financetracker;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.room.Room;

import com.example.financetracker.context.AppDatabase;
import com.example.financetracker.context.Income;
import com.example.financetracker.context.dao.IncomeDAO;

import java.util.ArrayList;

public class Fragment2 extends Fragment {

    ListView lstIncomes ;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment2,container,false);
        lstIncomes = view.findViewById(R.id.lstIncome);
        ArrayList descriptions =new ArrayList<>();
        ArrayList amounts =new ArrayList<>();
        ArrayList<Long> ids = new ArrayList<>();

        AppDatabase appDb = Room.databaseBuilder(this.getContext(), AppDatabase.class,"financeTracker").allowMainThreadQueries().build();
        IncomeDAO incomeDAO = appDb.incomeDAO();

        for (Income income:incomeDAO.getAll()){
            descriptions.add(income.getDescription());
            amounts.add(income.getAmount()+"");
            ids.add((income.getIncomeId()));
        }

//        String[] lstDescription = (String[])descriptions.toArray(new String[0]);
        String[] lstDescription = (String[]) descriptions.toArray(new String[0]);
        String[] lstAmounts = (String[]) amounts.toArray(new String[0]);
//        String[] lstAmounts = (String[])amounts.toArray(new String[0]);

        MyListAdapter adapter = new MyListAdapter(this.getActivity(),lstDescription,lstAmounts,ids);
        lstIncomes.setAdapter(adapter);


        return view;
    }
}
