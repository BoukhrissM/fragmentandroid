package com.example.financetracker;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.room.Room;

import com.example.financetracker.context.AppDatabase;
import com.example.financetracker.context.Expense;
import com.example.financetracker.context.dao.ExpenseDAO;

import java.util.ArrayList;

public class Fragment1 extends Fragment {
    ListView lstExpense;
    ExpenseDAO expenseDAO;
    ArrayList maintitle = new ArrayList();

    ArrayList<Long> ids = new ArrayList<>();
    ArrayList subtitle =new ArrayList();



    private Activity fragmentActivity;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            fragmentActivity = (Activity) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentActivity = null;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment1,container,false);

        lstExpense = view.findViewById(R.id.lstExpense);
        ArrayList maintitle = new ArrayList();

        ArrayList<Long> ids = new ArrayList<>();
        ArrayList subtitle =new ArrayList();


        AppDatabase db = Room.databaseBuilder(this.getContext(), AppDatabase.class,"financeTracker").allowMainThreadQueries().build();

        ExpenseDAO expenseDAO = db.expenseDAO();
        for (Expense expense:expenseDAO.getAll()) {
            maintitle.add(expense.getAmount()+"");
            subtitle.add(expense.getDescription());
            ids.add(expense.getExpenseID());
        }

        String[] titles = (String[]) maintitle.toArray(new String[0]);
        String[] sub_titles = (String[]) subtitle.toArray(new String[0]);

        MyListAdapter adapter = new MyListAdapter(this.getActivity(),titles,sub_titles,ids);
        lstExpense.setAdapter(adapter);


        lstExpense.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
                builder.setMessage("delete ?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                AppDatabase db = Room.databaseBuilder(getActivity().getApplicationContext(), AppDatabase.class, "financeTracker").allowMainThreadQueries().build();
                                ExpenseDAO expenseDAO = db.expenseDAO();

                                MyListAdapter myadapter = (MyListAdapter) lstExpense.getAdapter();
                                Expense expense = expenseDAO.findById(myadapter.getIds().get(position));
                                expenseDAO.delete(expense);

                                maintitle.clear();
                                subtitle.clear();
                                ids.clear();

                                for (Expense ex : expenseDAO.getAll()) {
                                    maintitle.add(ex.getAmount() + "");
                                    subtitle.add(ex.getDescription());
                                    ids.add(ex.getExpenseID());
                                }

                                String[] titles = (String[]) maintitle.toArray(new String[0]);
                                String[] sub_titles = (String[])subtitle.toArray(new String[0]);

                                myadapter = new MyListAdapter(fragmentActivity, titles, sub_titles, ids);
                                lstExpense.setAdapter(myadapter);
                                myadapter.notifyDataSetChanged();

                                Toast.makeText(getActivity().getApplicationContext(), "Deleted successfully", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                builder.create().show();
                return true;
            }
        });

        return view;
    }
}
